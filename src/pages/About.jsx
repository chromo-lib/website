import React from 'react'

export default function About() {
  return (
    <section className='bg-linear'>
      <h1>Free For Open-Source!</h1>
      <p className='lead'>ALL APPS AND EXTENSIONS ARE BUILT ON OPEN-SOURCE AND WOULDN'T EXIST WITHOUT IT.</p>
      <p className='lead'>WE'RE HAPPY TO GIVE SOMETHING BACK BY BEING COMPLETELY FREE FOR OPEN SOURCE.</p>
    </section>
  )
}

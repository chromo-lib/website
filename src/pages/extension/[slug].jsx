import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';

export default function Extenstion() {
  const { name } = useParams();
  const [extension, setExtension] = useState(null)

  useEffect(() => {
    fetch('https://cdn.statically.io/gl/haikelfazzani/hf_files/main/_browser-extensions.json')
      .then(r => r.json())
      .then(extensions => {
        const ext = extensions.find(e => e.name.includes(name));
        setExtension(ext)
      });
  }, []);

  if (extension) {
    return (
      <section>
        <h2 className='mb-0 blur'>{extension.name}</h2>
        <p className='lead slideRight'>{extension.description}</p>

        <div className='w-100 d-flex justify-center'>
          {extension.link.edge && <a href={extension.link.edge} className='btn' target="_blank"><i className='fab fa-edge mr-1'></i>Edge</a>}
          {extension.link.firefox && <a href={extension.link.firefox} className='btn' target="_blank"><i className='fab fa-firefox mr-1'></i>Firefox</a>}
          {extension.link.repository && <a href={extension.link.repository} className='btn' target="_blank"><i className='fa fa-code mr-1'></i>Code</a>}
        </div>

        <ul className='grid-2 text-center mt-5'>
          {extension.captures.map((img, i) => <li className='placeholder mb-3' key={i}>
            <img className="w-100 br7 blur" src={img} alt={extension.name} loading="lazy" />
          </li>)}
        </ul>
      </section>
    )
  }
  else return <></>
}

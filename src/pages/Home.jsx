import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { IKContext, IKImage } from 'imagekitio-react';
import ListExtensions from './ListExtensions';

export default function Home() {

  useEffect(() => {
    const targets = document.querySelectorAll('section');
    let options = { threshold: .25 }

    let observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.style.opacity = 1;

          entry.target.querySelector('h2').classList.add('blur');
          if(entry.target.querySelector('ul')) entry.target.querySelector('ul').classList.add('flipX');
          
          entry.target.querySelectorAll('section>p').forEach(p => p.classList.add('slideLeft'));

          observer.unobserve(entry.target);
        }
      });
    }, options);

    targets.forEach(target => {
      target.style.opacity = 0;
      observer.observe(target);
    });

    return () => {
      observer.disconnect();
    }
  }, []);

  return <>
    <section className='text-center bg-linear'>
      <p className='lead'>Chromo Lib</p>
      <h2 className='mb-0'>Build apps and extensions for the browser.</h2>
      <p className='lead'>All apps and extensions are built on open-source and wouldn't exist without it, we're happy to give something back by being completely free.</p>
      <div>
        <Link className='btn' to="/extensions">Apps & Extensions<i className='fa fa-arrow-right ml-1'></i></Link>
      </div>
    </section>

    <ListExtensions />

    <section className='mt-5 grid-2 bg-white'>
      <div>
        <h2>Why choosing Chromo Lib?</h2>
        <ul>
          <li><i className='fa fa-check mr-1'></i>Open source and free.</li>
          <li><i className='fa fa-check mr-1'></i>No unnecessary permissions.</li>
          <li><i className='fa fa-check mr-1'></i>No ads.</li>
          <li><i className='fa fa-check mr-1'></i>No tracking (privacy first).</li>
          <li><i className='fa fa-check mr-1'></i>We support almost all browsers.</li>
        </ul>
      </div>
      <IKContext urlEndpoint="https://ik.imagekit.io/xatykdb8hzb">
        <IKImage path="/browser_extensions/browsers_bkD1tlj7q.png" loading="lazy" lqip={{ active: true }} />
      </IKContext>
    </section>
  </>
}

import React from 'react';
import { Link } from 'react-router-dom';
import useExtensions from '../hooks/useExtensions';

export default function ListExtensions() {
  const extensions = useExtensions();

  return <>
    <section className='mt-5'>
      <h2 className='mb-0 text-center blur'>List of our extensions</h2>
      <p className='mt-0 text-center mb-3'>free and open source</p>
      {extensions && <ul className='grid-3 flipX'>
        {extensions.map((ext, i) => <li className='h-100 card' key={i}>
          <Link to={"/extension/" + ext.name}><h3>{ext.name}</h3></Link>
          <p className='justify'>{ext.description}</p>
        </li>)}
      </ul>}
    </section>
  </>
}

import { Link, Route, Routes } from "react-router-dom";
import About from './pages/About';
import Extenstion from "./pages/extension/[slug]";
import Home from './pages/Home';
import ListExtensions from "./pages/ListExtensions";

export default function App() {
  return <>
    {/* <nav className="grid-3">
      <ul className="d-flex">
        <li className="pl-0">
          <Link to="/">
            <h4 className="m-0 d-flex align-center"><i className="fas fa-shapes mr-2"></i>Chromo Lib</h4>
          </Link>
        </li>
      </ul>

      <ul></ul>

      <ul className="d-flex justify-end">
        <li><Link to="/"><i className="fa fa-home"></i></Link></li>
        <li><a href="https://github.com/Chromo-lib" target="_blank"><i className="fab fa-github"></i></a></li>
        <li><a href="https://gitlab.com/chromo-lib" target="_blank"><i className="fab fa-gitlab"></i></a></li>
        <li><a href="https://github.com/Chromo-lib" target="_blank"><i className="fab fa-twitter"></i></a></li>
      </ul>
    </nav> */}

    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/extensions" element={<ListExtensions />} />
      <Route path="/extension/:name" element={<Extenstion />} />
    </Routes>

    <footer className="w-100 bg-white  text-center mt-5">
      <ul className="d-flex justify-center">
        <li className="mr-2"><Link to="/"><i className="fa fa-home"></i></Link></li>
        <li className="mr-2"><a href="https://github.com/Chromo-lib" target="_blank"><i className="fab fa-github"></i></a></li>
        <li className="mr-2"><a href="https://gitlab.com/chromo-lib" target="_blank"><i className="fab fa-gitlab"></i></a></li>
        <li><a href="https://github.com/Chromo-lib" target="_blank"><i className="fab fa-twitter"></i></a></li>
      </ul>
      <p className="m-0 mr-1">Copyright © 2022 - Chromo Lib</p>
    </footer>
  </>
}

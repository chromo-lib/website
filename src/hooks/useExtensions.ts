import { useEffect, useState } from 'react'

export default function useExtensions() {
  const [extensions, setExtensions] = useState(null)

  useEffect(() => {
    fetch('https://cdn.statically.io/gl/haikelfazzani/hf_files/main/_browser-extensions.json')
      .then(r => r.json())
      .then(setExtensions);
  }, []);

  return extensions
}
